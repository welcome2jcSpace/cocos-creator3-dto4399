# CocosCreator3DTo4399

## Introduced  
Open source cocos Creator 3D released to 4399 web miniclip games  
Tool usage:  
1. Change the suffix of all. Cconb files in the project directory to. Bin  
2. Create a new CCONb pipeline to handle all.cconb dependencies that change their urls  
3. Rewrite bin Downloader to determine whether cCONB or bin is processed by url  

## Respository
[gitee](https://gitee.com/welcome2jcSpace/cocos-creator3-dto4399)

#### screenshots
![](./README_RES/game.png)

#### Error
![](./README_RES/error.png)


## core code
**Added a new resource pipeline to handle.cconb**
```javascript
  //新加一个额外处理cconb的资源管道
  function append_cconb_convert_pip() {
    const assetManager = cc.assetManager;
    //新建一个额外处理cconb的资源管道
    assetManager.transformPipeline.append(function (task) {
      const input = task.output = task.input;
      for (let item of input) {
        if (!item.url) continue;
        //更改资源依赖
        item.url.endsWith(".cconb") && ( item.url = cc.path.changeExtname(item.url, ".cconb.bin") );
      }
    });
    //重写bin资源加载处理
    assetManager.downloader.register("bin", (url, options, onComplete) => {
      url.endsWith(".cconb.bin") ? assetManager.downloader._downloaders[".cconb"]() : assetManager.downloader._downloaders[".bin"]();
    })
  }
```

**Change all. Cconb file suffixes**
```python
# 转换cconb格式 to bin
def ConvertConb2bin(dir):
    for root, dirs, files in os.walk(dir):
        path = root.replace('\\', '/')
        for f in files:
            url = '%s/%s' % (path, f)
            info = os.path.splitext(f)
            name = info[0]
            ext = info[1]
            if ext == ".cconb":
                os.rename(f"{root}/{f}", f"{root}/{name}.cconb.bin")
                print(f"{root}/{name}.cconb.bin")
```

**Inject pipelines into the project**
```python
# 注入脚本到项目中
def intoApplication(dir):
    content = IUtils.fromFile(f"{dir}/application.js")
    content = content.replace(
        "function topLevelImport(url) {", "%s\n\tfunction topLevelImport(url) {" % cconbPip, 1)
    content = content.replace("return loadAssetBundle(settings.hasResourcesBundle, settings.hasStartSceneBundle);",
                              "append_cconb_convert_pip();//创建cconb管道\n\treturn loadAssetBundle(settings.hasResourcesBundle, settings.hasStartSceneBundle);", 1)
    IUtils.writeInFile(f"{dir}/application.js",content)
```

## Packaged into EXE  

**Need to be installed in advance pyinstaller**

`pip install pyinstaller`

**Package command**  
 
'Open the terminal and type PyInstaller -f main.py'  
 
The generated exe is in the dist directory  
 
Error when calling exe  
You need to manually create a SRC directory in the exe directory and place your Web-mobile project in the SRC directory  
 
## Use method  
Cc package web-mobile into the SRC directory to run after the completion of the run  


## program entry
main.py

## python version
3.9.9

## Cocos Creator version
3.2.0
