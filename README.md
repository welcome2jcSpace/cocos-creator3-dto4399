# CocosCreator3DTo4399

## 介绍
开源 将cocos creator 3d 发布到4399网页小游戏
工具用途:
1. 更改项目目录下 所有.cconb后缀更改为.bin格式
2.  新建cconb管线处理所有 .cconb依赖更改其url 
3.  重写bin downloader 通过url判定处理 cconb 还是 bin 

## 仓库地址
[gitee](https://gitee.com/welcome2jcSpace/cocos-creator3-dto4399)

#### 运行成功截图
![](./README_RES/game.png)

#### 相关错误
![](./README_RES/error.png)


## 核心
**新增一条处理 .cconb的资源管线**
```javascript
  //新加一个额外处理cconb的资源管道
  function append_cconb_convert_pip() {
    const assetManager = cc.assetManager;
    //新建一个额外处理cconb的资源管道
    assetManager.transformPipeline.append(function (task) {
      const input = task.output = task.input;
      for (let item of input) {
        if (!item.url) continue;
        //更改资源依赖
        item.url.endsWith(".cconb") && ( item.url = cc.path.changeExtname(item.url, ".cconb.bin") );
      }
    });
    //重写bin资源加载处理
    assetManager.downloader.register("bin", (url, options, onComplete) => {
      url.endsWith(".cconb.bin") ? assetManager.downloader._downloaders[".cconb"]() : assetManager.downloader._downloaders[".bin"]();
    })
  }
```

**更改所有.cconb文件后缀**
```python
# 转换cconb格式 to bin
def ConvertConb2bin(dir):
    for root, dirs, files in os.walk(dir):
        path = root.replace('\\', '/')
        for f in files:
            url = '%s/%s' % (path, f)
            info = os.path.splitext(f)
            name = info[0]
            ext = info[1]
            if ext == ".cconb":
                os.rename(f"{root}/{f}", f"{root}/{name}.cconb.bin")
                print(f"{root}/{name}.cconb.bin")
```

**注入管线到项目中**
```python
# 注入脚本到项目中
def intoApplication(dir):
    content = IUtils.fromFile(f"{dir}/application.js")
    content = content.replace(
        "function topLevelImport(url) {", "%s\n\tfunction topLevelImport(url) {" % cconbPip, 1)
    content = content.replace("return loadAssetBundle(settings.hasResourcesBundle, settings.hasStartSceneBundle);",
                              "append_cconb_convert_pip();//创建cconb管道\n\treturn loadAssetBundle(settings.hasResourcesBundle, settings.hasStartSceneBundle);", 1)
    IUtils.writeInFile(f"{dir}/application.js",content)
```

## 打包成EXE

**需要提前安装 pyinstaller**

`pip install pyinstaller`

**打包命令**

`打开终端 输入 pyinstaller -f main.py`

生成的exe在dist目录下

### 调用exe报错
需要手动在exe目录下 创建src目录 并将你的 web-mobile项目放入 src目录下


## 使用方法
将 cc打包后的 web-mobile 放入 src目录下运行 运行完成后 即可


## 程序入口
main.py

## python 版本
3.9.9

## Cocos Creator 版本
3.2.0
